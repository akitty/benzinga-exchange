var app = angular.module('StarterApp', ['ngMaterial', 'ngMdIcons']);

app.controller('AppCtrl', ['$scope', '$http','$mdBottomSheet','$mdSidenav', '$mdDialog', '$log', function($scope, $http, $mdBottomSheet, $mdSidenav, $mdDialog, $log){
  // $scope.toggleSidenav = function(menuId) {
  //   $mdSidenav(menuId).toggle();
  // };
 	
  $scope.holdings = [];

    $scope.balance = 0;
    $scope.bidprice = 0;
    $scope.askprice = 0
    $scope.who=""
    $scope.symbol=""
    $scope.buysell="Buy"
    $scope.number=1
    $scope.invalidstock=false;
    $scope.nomoney=false;
    $scope.notenoughstocks=false;
    // $scope.

    $scope.getuser = function() {
        $http({method: "GET", url: "user/"+$scope.who}).
          then(function(response) {
            if (response.status== 200 ) {
              user=response.data
              $log.log(user);

              $scope.balance = user.balance;
              $scope.holdings = user.stocks;
            }
         
          }, function(response) {
            //error callback
        });    
      }

        $scope.lookup = function() {
        $http({method: "GET", url: "stocks/"+$scope.symbol}).
          then(function(response) {
            if (response.status== 200 ) {
              stock=response.data
              $log.log(stock);
              if (stock.symbol== "") {
                $scope.invalidstock=true;
                 $scope.bidprice=0
                  $scope.askprice=0
                return
              } else {
                 $scope.invalidstock=false;

              }
              $scope.bidprice = stock.bidPrice;
              $scope.askprice = stock.askPrice;

            }
         
          }, function(response) {
            //error callback
        });    
      }

      $scope.purchase = function() {
        $scope.nomoney=false;
          $scope.notenoughstocks=false;
        $log.log($scope.symbol,$scope.buysell,$scope.number,$scope.who)
        user = {"user":$scope.who}
        if( $scope.buysell == "Sell" ){
          methodt="DELETE"
        } else if( $scope.buysell == "Buy") {
          methodt="POST"
        }

        $http({method: methodt, 
          url: "stocks/"+$scope.symbol+"/"+$scope.number,
          params: user
        }).
          then(function(response) {
            if (response.status== 200 ) {
              $scope.nomoney=false;
              $scope.notenoughstocks=false;
              $scope.getuser()
              // $scope.balance = user.balance;
              // $scope.holdings = user.stocks;
            }
         
          }, function(response) {
            //error callback
            $log.log("http error?")
            if(response.status == 403) {
                 if( $scope.buysell == "Sell" ){
                  $scope.notenoughstocks=true;
                } else if( $scope.buysell == "Buy") {
                   $scope.nomoney=true;
                }
            }
        });    
      }

}]);


app.config(function($mdThemingProvider) {
  var customBlueMap = 		$mdThemingProvider.extendPalette('light-blue', {
    'contrastDefaultColor': 'light',
    'contrastDarkColors': ['50'],
    '50': 'ffffff'
  });
  $mdThemingProvider.definePalette('customBlue', customBlueMap);
  $mdThemingProvider.theme('default')
    .primaryPalette('customBlue', {
      'default': '500',
      'hue-1': '50'
    })
    .accentPalette('pink');
  $mdThemingProvider.theme('input', 'default')
        .primaryPalette('grey')
});