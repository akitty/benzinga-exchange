package exchange

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type StockJson struct {
	Symbol             string  `json:"symbol"`
	DxSymbol           string  `json:"dxSymbol"`
	Exchange           string  `json:"exchange"`
	IsoExchange        string  `json:"isoExchange"`
	BzExchange         string  `json:"bzExchange"`
	Type               string  `json:"type"`
	Name               string  `json:"name"`
	Description        string  `json:"description"`
	Sector             string  `json:"sector"`
	Industry           string  `json:"industry"`
	Open               float64 `json:"open"`
	High               float64 `json:"high"`
	Low                float64 `json:"low"`
	Close              float64 `json:"close"`
	BidPrice           float64 `json:"bidPrice"`
	AskPrice           float64 `json:"askPrice"`
	AskSize            int     `json:"askSize"`
	BidSize            int     `json:"bidSize"`
	Size               int     `json:"size"`
	BidTime            int64   `json:"bidTime"`
	AskTime            int64   `json:"askTime"`
	LastTradePrice     float64 `json:"lastTradePrice"`
	LastTradeTime      int64   `json:"lastTradeTime"`
	Volume             int     `json:"volume"`
	Change             float64 `json:"change"`
	ChangePercent      float64 `json:"changePercent"`
	PreviousClosePrice float64 `json:"previousClosePrice"`
	FiftyTwoWeekHigh   float64 `json:"fiftyTwoWeekHigh"`
	FiftyTwoWeekLow    float64 `json:"fiftyTwoWeekLow"`
	MarketCap          int64   `json:"marketCap"`
	SharesOutstanding  int64   `json:"sharesOutstanding"`
	Pe                 float64 `json:"pe"`
	ForwardPE          float64 `json:"forwardPE"`
	DividendYield      float64 `json:"dividendYield"`
	PayoutRatio        float64 `json:"payoutRatio"`
	EthPrice           float64 `json:"ethPrice"`
	EthVolume          int     `json:"ethVolume"`
	EthTime            int64   `json:"ethTime"`
}

type SymbolSource interface {
	GetSymbol(string) (*StockJson, error)
	GetSymbols([]string) (map[string]StockJson, error)
}

type MockSymbolSource struct{}

func JsonToStocksMap(b []byte) (map[string]StockJson, error) {
	var m map[string]StockJson
	err := json.Unmarshal(b, &m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func (t MockSymbolSource) GetSymbol(s string) (*StockJson, error) {
	bytes, err := ioutil.ReadFile("sample.json")
	if err != nil {
		return nil, err
	}
	m, err := JsonToStocksMap(bytes)
	if err != nil {
		return nil, err
	}
	stock := m["F"]
	return &stock, nil
}

func (t MockSymbolSource) GetSymbols(s []string) (map[string]StockJson, error) {
	bytes, err := ioutil.ReadFile("samplemulti.json")

	if err != nil {
		return nil, err
	}
	m, err := JsonToStocksMap(bytes)
	if err != nil {
		return nil, err
	}
	return m, nil
}

const BenzingaUrl = "http://careers-data.benzinga.com/rest/richquoteDelayed"

type BenzingaSource struct{}

func (b BenzingaSource) GetSymbol(s string) (*StockJson, error) {
	url, err := url.Parse(BenzingaUrl)
	if err != nil {
		return nil, err
	}

	q := url.Query()
	q.Set("symbols", s)
	url.RawQuery = q.Encode()

	resp, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	m, err := JsonToStocksMap(bytes)
	if err != nil {
		return nil, err
	}
	stock := m[s]
	return &stock, nil

}
func (b BenzingaSource) GetSymbols(s []string) (map[string]StockJson, error) {
	url, err := url.Parse(BenzingaUrl)
	if err != nil {
		return nil, err
	}

	q := url.Query()

	q.Set("symbols", strings.Join(s, ","))
	url.RawQuery = q.Encode()

	resp, err := http.Get(url.String())
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	m, err := JsonToStocksMap(bytes)
	if err != nil {
		return nil, err
	}

	return m, nil
}
