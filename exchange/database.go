package exchange

import (
	"database/sql"
	"fmt"
	"log"
)

type database struct {
	db *sql.DB
}

func NewDatabase(fileName string) (*database, error) {
	d := database{}
	db, err := sql.Open("sqlite3", fileName)
	if err != nil {
		log.Fatal(err)
	}
	d.db = db

	createHoldingsStmt := `CREATE TABLE IF NOT EXISTS holdings (
		id INTEGER not null primary key, 
		symbol TEXT, 
		number INTEGER,
		holder_name TEXT 
	);
	CREATE INDEX IF NOT EXISTS user_name on holdings(holder_name);
	`

	_, err = db.Exec(createHoldingsStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, createHoldingsStmt)
		return nil, err
	}

	return &d, nil
}

func (d database) GetUserStocks(user string) (*User, error) {
	s := `select symbol,SUM(number) from holdings
	where holder_name is "%s"
	group by symbol,holder_name ;`
	s = fmt.Sprintf(s, user)
	rows, err := d.db.Query(s)
	defer rows.Close()
	if err != nil {
		log.Printf("%q: %s\n", err, s)
		return nil, err
	}

	u := &User{user, make([]Stock, 0), 0}
	for rows.Next() {
		var symbol string
		var sum int

		err = rows.Scan(&symbol, &sum)
		if err != nil {
			return nil, err
		}
		if symbol == "cash_money" {
			u.Balance = sum
		} else {
			u.Stocks = append(u.Stocks, Stock{symbol, sum})
		}

	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	if len(u.Stocks) == 0 && u.Balance == 0 {
		return nil, fmt.Errorf("user does not exist")
	}

	return u, nil
}

func (d database) CreateUser(user string) error {
	s := `insert into holdings(symbol,number,holder_name) values("cash_money",10000000,"%s");`
	s = fmt.Sprintf(s, user)
	_, err := d.db.Exec(s)
	if err != nil {
		log.Printf("%q: %s\n", err, s)
		return err
	}

	return nil
}

const (
	BUY = iota
	SET
)

func (d database) UpdateUser(user, symbol string, num int, diff int) error {

	tx, err := d.db.Begin()
	if err != nil {
		log.Print(err)
		tx.Rollback()
		return err
	}

	s := `insert into holdings(holder_name,symbol,number) values("%s","%s",%d);`
	s = fmt.Sprintf(s, user, symbol, num)

	_, err = tx.Exec(s)
	if err != nil {
		log.Print(err)
		tx.Rollback()
		return err
	}

	s = `UPDATE holdings SET number = number - %d WHERE holder_name="%s" AND symbol = "cash_money"`
	s = fmt.Sprintf(s, diff, user)
	_, err = tx.Exec(s)
	if err != nil {
		log.Print(err)
		tx.Rollback()
		return err
	}

	s = `SELECT number FROM holdings  WHERE holder_name="%s" AND symbol = "cash_money"`
	s = fmt.Sprintf(s, user)
	row := tx.QueryRow(s)
	var newBalance int
	err = row.Scan(&newBalance)
	if err != nil {
		log.Print(err)
		tx.Rollback()
		return err
	}

	if newBalance < 0 {
		tx.Rollback()
		return fmt.Errorf("cant go under 0 balance:%d", newBalance)
	}

	s = `select SUM(number) from holdings
	where holder_name="%s" AND symbol="%s";`
	s = fmt.Sprintf(s, user, symbol)
	row = tx.QueryRow(s)
	var numberLeft int
	err = row.Scan(&numberLeft)
	if err != nil {
		log.Print(err)
		tx.Rollback()
		return err
	}

	if numberLeft < 0 {
		tx.Rollback()
		return fmt.Errorf("cant over sell %s, left :%d", symbol, numberLeft)
	}

	tx.Commit()

	return nil
}

func (d database) DeleteUser(user string) error {
	s := `DELETE from holdings where holder_name="%s";`
	s = fmt.Sprintf(s, user)
	_, err := d.db.Exec(s)
	if err != nil {
		log.Printf("%q: %s\n", err, s)
		return err
	}

	return nil
}
