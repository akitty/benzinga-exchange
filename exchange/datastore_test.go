package exchange

import (
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDB(t *testing.T) {
	os.Remove("db")
	var err error
	db, err := NewDatabase("db")
	assert.NoError(t, err)

	err = db.CreateUser("pat")
	assert.NoError(t, err)

	user, err := db.GetUserStocks("pat")

	assert.NoError(t, err)

	assert.NotNil(t, user)
	assert.Equal(t, "pat", user.Name)
	assert.Equal(t, 10000000, user.balance)

	err = db.UpdateUser("pat", "AAA", 51, 1)
	assert.NoError(t, err)

	user, err = db.GetUserStocks("pat")
	assert.NoError(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, "pat", user.Name)
	assert.Equal(t, 10000000-1, user.balance)
	assert.Len(t, user.stocks, 1)
	assert.Equal(t, user.stocks[0].Symbol, "AAA")
	assert.Equal(t, user.stocks[0].Number, 51)

	err = db.UpdateUser("pat", "BB", 55, 10e10)
	assert.Error(t, err)

	user, err = db.GetUserStocks("pat")

	err = db.UpdateUser("pat", "BB", -10, -10e5)
	assert.Error(t, err)

	err = db.DeleteUser("pat")
	assert.NoError(t, err)

	user, err = db.GetUserStocks("pat")
	log.Printf("%+v", user)
}
