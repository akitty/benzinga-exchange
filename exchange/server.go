package exchange

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
}

type Stock struct {
	Symbol string `json:"symbol"`
	Number int    `json:"number"`
}

type User struct {
	Name    string  `json:"name"`
	Stocks  []Stock `json:"stocks"`
	Balance int     `json:"balance"` //cents
}

type Exchange struct {
	router *gin.Engine
	api    SymbolSource
	db     *database
}

func (this *Exchange) Start() {
	log.Fatal(this.router.Run(":8080"))
}

func NewServer() *Exchange {
	s := &Exchange{}
	s.api = BenzingaSource{}
	var err error
	s.db, err = NewDatabase("exchange.db")
	if err != nil {
		return nil
	}

	s.router = gin.Default()

	stocks := s.router.Group("/stocks")
	{
		stocks.GET("/:name", s.Getstocks)
		stocks.POST("/:symbol/:amount", s.Buystocks)
		stocks.DELETE("/:symbol/:amount", s.Sellstocks)
	}

	users := s.router.Group("/user")
	{
		users.GET("/:name", s.GetUser)
		users.POST("/:name", s.MakeUser)
		users.DELETE("/:name", s.DeleteUser)

	}

	s.router.Static("/js", "./assets/js")
	s.router.Static("/css", "./assets/css")

	s.router.StaticFile("/", "./assets/index.html")
	s.router.StaticFile("/favicon.ico", "./assets/favicon.ico")

	// s.router.GET("/", ...)

	return s
}

type StockJsonSimple struct {
	Symbol   string  `json:"symbol"`
	BidPrice float64 `json:"bidPrice"`
	AskPrice float64 `json:"askPrice"`
}

func (e Exchange) Getstocks(c *gin.Context) {
	name := c.Param("name")
	stock, err := e.api.GetSymbol(strings.ToUpper(name))
	if err != nil {
		c.Error(err)
	}
	simple := StockJsonSimple{
		Symbol:   stock.Symbol,
		BidPrice: stock.BidPrice,
		AskPrice: stock.AskPrice,
	}
	respString, err := json.Marshal(simple)
	c.String(http.StatusOK, string(respString))
}

func (e Exchange) Buystocks(c *gin.Context) {
	symbol := c.Param("symbol")
	amount := c.Param("amount")
	user := c.Query("user")

	num, err := strconv.Atoi(amount)
	if err != nil {
		c.Error(err)
		return
	}

	stock, err := e.api.GetSymbol(symbol)
	if err != nil {
		c.Error(err)
		return
	}

	diff := num * int(stock.AskPrice*100)

	err = e.db.UpdateUser(user, symbol, num, diff)
	if err != nil {
		if strings.Contains(err.Error(), "under") {
			c.String(http.StatusForbidden, "not enough money")
		}
	}

}
func (e Exchange) Sellstocks(c *gin.Context) {
	symbol := c.Param("symbol")
	amount := c.Param("amount")
	user := c.Query("user")

	num, err := strconv.Atoi(amount)
	if err != nil {
		c.Error(err)
		return
	}
	num *= -1

	stock, err := e.api.GetSymbol(symbol)
	if err != nil {
		c.Error(err)
		return
	}

	diff := num * int(stock.BidPrice*100)

	err = e.db.UpdateUser(user, symbol, num, diff)
	if err != nil {
		if strings.Contains(err.Error(), "sell") {
			c.String(http.StatusForbidden, "not enough stocks")
		}
	}

}
func (e Exchange) GetUser(c *gin.Context) {
	user := c.Param("name")
	u, err := e.db.GetUserStocks(user)
	if err != nil {
		if err.Error() == "user does not exist" {
			err = e.db.CreateUser(user)
			if err != nil {
				c.Error(err)
				return
			} else {
				u, err := e.db.GetUserStocks(user)
				if err != nil {
					c.Error(err)
					return
				}
				c.JSON(http.StatusOK, u)
				return
			}
		}
		c.Error(err)
		return
	} else {
		c.JSON(http.StatusOK, u)
	}
}
func (e Exchange) MakeUser(c *gin.Context) {
	user := c.Param("name")
	err := e.db.CreateUser(user)
	if err != nil {
		c.Error(err)
	} else {
		c.String(http.StatusOK, "")
	}
}

func (e Exchange) DeleteUser(c *gin.Context) {
	user := c.Param("name")
	err := e.db.DeleteUser(user)
	if err != nil {
		c.Error(err)
	} else {
		c.String(http.StatusOK, "")
	}
}
