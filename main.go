package main

import (
	"benzinga/exchange"
	"log"
)

func main() {
	log.Println("start")
	e := exchange.NewServer()
	e.Start()
}
